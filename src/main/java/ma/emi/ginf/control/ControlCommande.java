package ma.emi.ginf.control;

import java.util.ArrayList;
import java.util.List;

import ma.emi.ginf.domain.Catalogue;
import ma.emi.ginf.domain.Produit;

public class ControlCommande {
    private Catalogue catalogue;
	private List<Commande> commandes;
	private Commande cc;
	private Map<Integer,Client>clients;
	
	 public ControlCommande(Catalogue c) {
		 this.catalogue =c;
	 }
	 
	 List<Produit> getAllProducts(){
		 List<Produit> result = new ArrayList<Produit>();
		   for(Produit p : this.catalogue.afficherListProduit())
			   	result.add(p);
		 
		 return  result;
	 }
     public Commande CreateCommande(int clientID){
		if(this.clients.contains(clientID)){
			Client cli= this.clients.get(clientID);
			Commande neWCC= new Commande(cli)
			this.commandes.add(newCC);
			this.cli.addCommande(newCC)
			this.cc=newCC;
		}
		return cc;
	 }

  public void addProduit(int qte, int itemId){

			Produit p = this.catalogue.chercherProduitByItemId(itemId);
			this.cc.createDetail(qte,p);
  }

}
